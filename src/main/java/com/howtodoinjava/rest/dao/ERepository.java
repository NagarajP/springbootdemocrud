package com.howtodoinjava.rest.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.howtodoinjava.rest.model.Employee;

@Repository
public interface ERepository extends JpaRepository<Employee, Integer> {

}
